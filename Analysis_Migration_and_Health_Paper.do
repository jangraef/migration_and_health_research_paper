/*==============================================================================
File name:    Migration & health Analysis
Class:        Seminar Migration & Health
Author:		  Jan Graef
Data:		  SOEP-Core, v36 (EU Edition), doi:10.5684/soep.core.v36eu
Last update:  03.07.2021
==============================================================================*/

/*------------------------------------------------------------------------------
                     #1. Configuring dofile
------------------------------------------------------------------------------*/

* Setup
version 16.1           // Stata version control (put your own version)
clear all             // clear working memory
macro drop _all       // clear macros

* Working directory
global wdir  "INSERT_WORKING_DIRECTORY_HERE"


* Define paths to subdirectories (don't change anything here)
global data 		"$wdir/0_data"   		// folder for original data
global code 		"$wdir/1_dofiles"   	// folder for do-files
global posted 		"$wdir/2_posted"    	// data ready for analysis
global temp 		"$wdir/3_temp"   		// folder for temporary files
global table		"$wdir/4_tables" 		// folder for table output 
global graph		"$wdir/5_graphs" 		// folder for graph output 

* Packages: 
findit grc1leg2	
ssc install fre, replace
ssc install outreg2, replace

/*-----------------------------------------------------------------------------
PART 1: DATA PREPARATION
------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
(I) MERGE DATA
------------------------------------------------------------------------------*/

use "$data/pl", clear
label language EN
keep pid syear hid plh0182 plb0021 plb0022_h plb0037_h ///
	plb0041 plb0072 plb0073_h plb0240 plj0014_v1 ///
	plj0071 plj0072 plj0073 plj0499 plb0424_v2 /// 
	plj0535 plj0554 plj0553 plj0556 ple0008 plj0551 ///
	plj0564 plj0588 plj0589 plj0592 plj0587 plj0622 ///
	plj0623 plj0624 plj0625 plj0654 plj0674 plj0677 plj0680_v1 ///
	plj0680_v2 plj0681 plj0707 plm0546 plm0625 plm0545 plb0072 ///
	plm0508 plm0509 plm0529 plm0533i02 plm0545 plh0182 plb0417_v2 ///
	plj0626 plj0627 plj0629 plj0630 plm0707 plm0708i01 plm0708i02 plm0681 ///
	plm0682 plm0683
	
* ppath dataset: merge 1:1 (identifier: pid syear)
merge m:1 pid using "$data/ppath", ///
	keepusing(sex gebjahr psample immiyear corigin) ///
	keep(1 3) gen(merge_ppath)
	
* Restriction 1: Keep only refugee samples (M3 & M4 & M5)
keep if psample > 16 & psample < 20
* Initial sample: n = 18342; m = 8321

* hl dataset: merge m:1 (identifier: hid syear)
merge m:1 hid syear using "$data/hl", ///
	keepusing(hlj0005_h) keep(1 3) gen(merge_hl)
	
* bioimmig dataset: merge 1:1 (identifier: pid syear)
merge 1:1 pid syear using "$data/bioimmig", ///
	keepusing(biimgrp biresper bicamp) keep(1 3) gen(merge_bioimmig)
		
* biol dataset: merge 1:1 (identifier: pid syear)
merge 1:1 pid syear using "$data/biol", ///
	keepusing(lr3034 lr3122 lr3136 lr3168 lr3235 lr3236 lr3237 lr3238 lr3240 ///
	lr3046 lr3041 lr3241 lr3032 lr3034 lr3036 lb0248 lr2110 lr3051 lr3343 ///
	lr3192) keep(1 3) gen(merge_biol)
		
* health dataset: merge 1:1 (identifier: pid syear)
merge 1:1 pid syear using "$data/health", ///
	keepusing(mcs pcs) keep(1 3) gen(merge_health)
	
* pequiv dataset: merge 1:1 (identifier: pid syear)
merge 1:1 pid syear using "$data/pequiv", ///
	keepusing(d11102ll x11104ll d11107 d11108 d11109 e11103 e11102 e11104 ///
	e11105_v1 m11126 asyl fasyl p11101) keep(1 3) gen(merge_pequiv) 
	
* pgen dataset: merge 1:1 (identifier: pid syear)
merge 1:1 pid syear using "$data/pgen", ///
	keepusing(pgstatus_asyl pgstatus_refu pgemplst pglfs ///
	pgerljob pgerwzeit pgtatzeit pgnace pgcasmin pgpsbila pgmonth pgpbbila) ///
	keep(1 3) gen(merge_pgen)
	
* Set label language to english
label language EN

* Save merged dataset

save "$posted/MH_merged", replace		
	

/*-----------------------------------------------------------------------------
(II) PREPARE VARIABLES
------------------------------------------------------------------------------*/

clear all             			
	use "$posted/MH_merged", clear
	
/*-----------------------------------------------------------------------------
(IIa) Outcome variables
------------------------------------------------------------------------------*/

* (i) Mental health scale (mcs)
replace mcs = . if mcs < 0
label variable mcs "Mental Health (MCS)"
table syear, c(mean mcs) // 2017 & 2019 not asked

* (ii) Self rated health
replace m11126 = . if m11126 < 0
gen sr_health = 5 - m11126 // higher values -> better health
label var sr_health "Current Self-Rated Health Status"
label define sr_health 0 "Bad" 1 "Poor" 2 "Satisfactory" 3 "Good" 4 "Very good"
label values sr_health sr_health

/*-----------------------------------------------------------------------------
(IIb) Other variables
------------------------------------------------------------------------------*/

* (i) Gender
gen female = sex - 1
label variable female "Female"
label define female 0 "Men" 1 "Women"
label values female female
recode female (-4 -2 =.)

* (ii) Age
replace gebjahr = . if gebjahr < 0
gen age = syear - gebjahr
label variable age "Age"

* (iii) Year of immigration
fre immiyear
replace immiyear = . if immiyear < 0
label var immiyear "Year of Immigration"

* (iv) Country of origin
recode corigin (30 = 1 "Syria") (60 = 2 "Iraq") (43 = 3 "Afghanistan") ///
	(24 = 4 "Iran") (85 = 5 "Pakistan") (89 = 6 "Eritrea") (84 = 7 "Somalia") ///
	(-2 -1 = .) (else = 8 "Other"), gen(cor)
tab corigin cor, m
label var cor "Country of origin"

* (v) Education
recode 	pgcasmin ///
		(-1 	 = . )					///
		(0       = 1 "In school")       ///
		(1 2 4   = 2 "Lower")			///
		(3 5 6   = 3 "Intermediate")	///
		(7 8 9   = 4 "Higher")			///
		, gen(edu)
label variable edu "Education"

* (vi) German language skills
recode plj0071 plj0072 plj0073 (-5 / 0 = .)
gen gerlan = ((5-plj0071) + (5-plj0072) + (5-plj0073))/3 
label var gerlan "German language index"
label define gerlan 0 "None" 4 "Very good"
label values gerlan gerlan

* (vii) Employment status
replace pgemplst =. if pgemplst <0
rename pgemplst empstat

* (vii) Legal status
gen legal_status = . 
replace legal_status = 1 if inlist(plj0680_v1,1) 
replace legal_status = 2 if inlist(plj0680_v1,2,3,4,6,7) 
replace legal_status = 3 if inlist(plj0680_v1,5) 
replace legal_status = 4 if inlist(plj0680_v1,8) 

replace legal_status = 1 if inlist(plj0680_v2,1,9) 
replace legal_status = 2 if inlist(plj0680_v2,2,3,4,6,7) 
replace legal_status = 3 if inlist(plj0680_v2,5,11) 
replace legal_status = 4 if inlist(plj0680_v2,10) 

lab var legal_status "Protection status"
lab def legal_status 1"In process" ///
	2 "Protection granted" ///
	3 "Protection denied"  4 "Other status" 
lab val legal_status legal_status

* (ix) Accomodation type: 
replace hlj0005_h = . if hlj0005_h < 0
gen accom = hlj0005_h if hlj0005_h > 0
recode accom 3 = 1
lab var accom "Accomodation type"
label def accom 1 "Shared accom./other" 2 "Private flat/house"
label val accom accom

* (Extra) Work-related gender egalitarianism
recode plj0622 plj0623 (-10/0=.)
recode plj0622 									///
	(1/3 = 0 "Disagree") 						///
	(4   = 1 "Neutral") 						///
	(5/7 = 2 "Agree"), gen(ega1)

recode plj0623									///
	(1/3 = 0 "Disagree") 						///
	(4   = 1 "Neutral") 						///
	(5/7 = 2 "Agree"), gen(ega2)


/*-----------------------------------------------------------------------------
(III) SAMPLE SELECTION & ANCHORING EVENT IN TIME
------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
(IIIa) Sample restrictions (1)
------------------------------------------------------------------------------*/

distinct pid  	//     			 Initial sample size: 	n = 18342; m = 8321

* (i) Age range
keep if inrange(age,18,60) 							// 	n = 17979; m = 8160

* (ii) Immigration after 2010
keep if inrange(immiyear,2010,2018)					// 	n = 17600; m = 7861

* (iii) Keep if at least 2 observations 
bysort pid: gen n = _n
bysort pid: egen N = max(n)
keep if N > 1   //   Preliminary analytic sample size: 	n = 15023; m = 5284

/*-----------------------------------------------------------------------------
(IIIb) Select treated (employment) and control (not employed) subsamples
------------------------------------------------------------------------------*/

* (i) Employment indicator
fre empstat
gen emp = (empstat == 1 | empstat == 2 | empstat == 3 | empstat == 4)
* Employment comprises:
*	- full time work, 
*   - part time work,
*   - vocational training,
*   - marginal irregular work

* (ii) Keep only persons "at risk" of getting employed 
* 	- i.e., not emloyed at first observation
gen aux = (n==1 & empstat == 5)
bysort pid: egen aux2 = max(aux)
keep if aux2 == 1 
distinct pid 	//							 			n = 13489; n = 4739

* (iii) Treated (experienced employment) variable
bysort pid: egen treated = max(emp)
label define treated 0 "Control" 1 "Treated"
label values treated treated

* Check: 
egen pickone = tag(pid)
tab treated if pickone == 1 
tab treated

* (iv) Homogenous control sample: Keep only labor force
drop if pglfs == 2 & treated == 0 					// age 65 +
drop if (pglfs == 3 | edu == 1) & treated == 0	 	// currently in education 
drop if pglfs == 4 & treated == 0 					// parental leave

distinct pid    // 										n = 12422; m = 4697

/*-----------------------------------------------------------------------------
(IIIc) Anchor event (employment) in time
------------------------------------------------------------------------------*/

* (i) Anchor event in time
gen aux3 = syear if emp == 1
bysort pid: egen emp1 = min(aux3)
label variable emp1 "Year of first employment"

* control: 
order pid syear empstat aux aux3 emp emp1
browse

* (ii) Time dummies for years before and after employment event
bysort pid (syear): gen yemp_index = syear - emp1

recode yemp_index ///
	(min/-1 . = 0 "Before employment") ///
	(	  0   = 1 "Year of employment") ///
	(	  1   = 2 "1 year after employment") ///
	(     2   = 3 "2 years after employment") ///
	, gen (time_dummies)
label var time_dummies "Time dummies (first employment)"

* control: 
order pid syear empstat aux aux3 emp emp1 yemp_index time_dummies
browse

/*-----------------------------------------------------------------------------
(IIId) Final sample restrictions
------------------------------------------------------------------------------*/

distinct pid    								// 		n = 12422; m = 4697
drop if legal_status == . 						//      n = 12222; m = 4679
drop if accom == . 								// 		n = 12185; m = 4677
drop if sr_health == .							//		n = 12164; m = 4675
drop if gerlan == .								// 		n = 12157; m = 4672

* keep if at least 2 observations after sample restrictions
capture drop n N
bysort pid: gen n = _n
bysort pid: egen N = max(n)
keep if N > 1 
tab treated female if n == 1, row

* Analytic sample size: 						// 		n = 11681; m = 4196
*	- differs by outcome variables (for MCS smaller)

/*-----------------------------------------------------------------------------
IV KEEP ONLY RELEVANT VARIABLES & SAVE ANALYTIC SAMPLE
------------------------------------------------------------------------------*/
keep 																///
	pid syear hid													///
	mcs																///
	sr_health 														///
	female 															///
	age	immiyear													///
	cor 															///
	edu 															///
	gerlan 															///
	empstat	emp1 yemp_index											///
	legal_status 													///
	accom 															///
	ega1 ega2														///
	treated time_dummies 


save "$posted/MH_Analytic", replace		
	
********************************************************************************

/*-----------------------------------------------------------------------------
PART 2: ANALYSIS
------------------------------------------------------------------------------*/

* Load analytic sample data	
	clear all             			
	use "$posted/MH_Analytic", clear
	
/*-----------------------------------------------------------------------------
(I) DESCRIPTION
------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
(Ia) Summary statistics table: Table 1
------------------------------------------------------------------------------*/

* (i) Generate dummy variables for categorical variables & missings

* Mental Health Scale (MCS)
* 6543 missing observations


* Self-rated health
* no missings

* Gender
* no missings

* Year of immigration
* no missings

* Country of origin
tab cor, gen(c)
label var c1 "\hspace{0.25cm} Syria"
label var c2 "\hspace{0.25cm} Iraq"
label var c3 "\hspace{0.25cm} Afghanistan"
label var c4 "\hspace{0.25cm} Iran"
label var c5 "\hspace{0.25cm} Pakistan"
label var c6 "\hspace{0.25cm} Eritrea"
label var c7 "\hspace{0.25cm} Somalia"
label var c8 "\hspace{0.25cm} Other"
* no missings

* Physical health scale (pcs)
* 6810 missing observations

* Education
tab edu, m gen(e)
label variable e1 "\hspace{0.25cm} In School"
label variable e2 "\hspace{0.25cm} Lower"
label variable e3 "\hspace{0.25cm} Intermediate"
label variable e4 "\hspace{0.25cm} Higher"
label variable e5 "\hspace{0.25cm} Missing"

* German language
* no missings

* Legal status
tab legal_status, gen(p)
label variable p1 "\hspace{0.25cm} In process"
label variable p2 "\hspace{0.25cm} Protection granted"
label variable p3 "\hspace{0.25cm} Protection denied"
label variable p4 "\hspace{0.25cm} Other status"
* no missings

* Accomodation
tab accom, gen(ac)
label var ac1 "\hspace{0.25cm} Shared accom./ Other" 
label var ac2 "\hspace{0.25cm} Private flat/ house"
* no missings

* Year of first employment
fre emp1

* Employment type (first employment)
gen e_type = . 
replace e_type = empstat if time_dummies == 1
label define e_type 			///
	1 "Full-time"				///
	2 "Regular part-time"		///
	3 "Vocational Training"		///
	4 "Marginal/ irregular part-time"
label val e_type e_type
bysort pid: egen etype = max(e_type)
label val etype e_type
tab etype, m gen(et)
label var et1 "\hspace{0.25cm} Full-time"
label var et2 "\hspace{0.25cm} Regular part-time"
label var et3 "\hspace{0.25cm} Vocational Training"
label var et4 "\hspace{0.25cm} Marginal / Irregular"
label var et5 "\hspace{0.25cm} Missing"

* Years of observation
capture drop n
bysort pid: gen n = _n
bysort pid: egen N = max(n)
gen yobs = N
label var yobs "Years observed"


* (ii) Generate indicator variable to split table by treated & gender

tab treated female if n == 1, m
gen auxsum = .
replace auxsum = 0 if female == 0 & treated == 0
replace auxsum = 1 if female == 0 & treated == 1
replace auxsum = 2 if female == 1 & treated == 0
replace auxsum = 3 if female == 1 & treated == 1
fre auxsum if n == 1


* (iii) Produce table
eststo table1: estpost tabstat 									///
	mcs sr_health												///
	age c1 c2 c3 c4 c5 c6 c7 c8 immiyear						///
	e1 e2 e3 e4 e5 gerlan										///
	p1 p2 p3 p4 ac1 ac2 										///
	et1 et2 et3 et4 et5 yobs 									///
	if n == 1, by(auxsum)										///
	statistics(mean sd count) nototal columns(statistics)
	
esttab table1 using "$table/table1.tex", compress label replace			///
 refcat(	c1 "\emph{Country of origin}"								///
			e1 "\emph{Education}"										///
			p1 "\emph{Protection status}"								///
			ac1 "\emph{Accomodation type}"								///
			et1 "\emph{Employment type}"								///
			, nolabel) 													///
 cells("mean(fmt(2)) count(fmt(0))") 									///
 collabels("M" "N")														///
 mtitle ("treated" "control") 											///
 wide not nostar unstack nonumber noobs nodepvars						///
 note(All statistics for time-changing variables are calculated for 	///
 respondents' first observation in the panel. Source: SOEP-Core,		///
 v36 (EU Edition), doi:10.5684/soep.core.v36eu)  
	
* Table A1: Summary statistics for non-missing values on mcs (?)
* gen sample1 = !missing(mcs)
* distinct pid if sample1 == 1


/*-----------------------------------------------------------------------------
(II) Models (Fixed-Effects)
------------------------------------------------------------------------------*/

* Declare panel data
xtset pid syear

* Relabel time_dummies for nice graphics
label def time_dummies2 0 "<0" 1 "0" 2 "+1" 3 "+2"
label val time_dummies time_dummies2

/*-----------------------------------------------------------------------------
(IIa) Overall models (no gender differentiation)
------------------------------------------------------------------------------*/

est clear

* (i) MCS

// Without control variables
xtreg 	mcs 					///
		i.time_dummies,			///
	fe vce(robust)
margins time_dummies, post noestimcheck
est store o_mcs_nc

// Model 1: With control variables
xtreg 	mcs 					///
		i.time_dummies			///
		age 					///
		gerlan					///
		i.legal_status			///
		i.accom,				///
	fe vce(robust)
est store m1
margins time_dummies, post noestimcheck
est store o_mcs_c


* (ii) Self-rated health status

// Without control variables
xtreg 	sr_health 				///
		i.time_dummies,			///
	fe vce(robust)
margins time_dummies, post noestimcheck
est store o_srh_nc

// Model 2: With control variables
xtreg 	sr_health 				///
		i.time_dummies			///
		age 					///
		gerlan					///
		i.legal_status			///
		i.accom,				///
	fe vce(robust)
est store m2
margins time_dummies, post noestimcheck
est store o_srh_c


/*-----------------------------------------------------------------------------
(IIb) Models for men only
------------------------------------------------------------------------------*/

* (i) MCS

// Without control variables
xtreg 	mcs 							///
		i.time_dummies					///
	if female == 0, fe vce(robust)
margins time_dummies, post noestimcheck
est store m_mcs_nc

// Model 3: With control variables
xtreg 	mcs 							///
		i.time_dummies					///
		age 							///
		gerlan							///
		i.legal_status					///
		i.accom							///
	if female == 0, fe vce(robust)
est store m3
margins time_dummies, post noestimcheck
est store m_mcs_c


* (ii) Self-rated health status

// Without control variables
xtreg 	sr_health 						///
		i.time_dummies					///
	if female == 0, fe vce(robust)
margins time_dummies, post noestimcheck
est store m_srh_nc

// Model 4: With control variables
xtreg 	sr_health 						///
		i.time_dummies					///
		age 							///
		gerlan							///
		i.legal_status					///
		i.accom							///
	if female == 0, fe vce(robust)
est store m4
margins time_dummies, post noestimcheck
est store m_srh_c


/*-----------------------------------------------------------------------------
(IIc) Models for women only
------------------------------------------------------------------------------*/

* (i) MCS

// Without control variables
xtreg 	mcs 							///
		i.time_dummies					///
	if female == 1, fe vce(robust)
margins time_dummies, post noestimcheck
est store f_mcs_nc

// Model 5: With control variables
xtreg 	mcs 							///
		i.time_dummies					///
		age 							///
		gerlan							///
		i.legal_status					///
		i.accom							///
	if female == 1, fe vce(robust)
est store m5
margins time_dummies, post noestimcheck
est store f_mcs_c


* (ii) Self-rated health status

// Without control variables
xtreg 	sr_health 						///
		i.time_dummies					///
	if female == 1, fe vce(robust)
margins time_dummies, post noestimcheck
est store f_srh_nc

// Model 6: With control variables
xtreg 	sr_health 						///
		i.time_dummies					///
		age 							///
		gerlan							///
		i.legal_status					///
		i.accom							///
	if female == 1, fe vce(robust)
est store m6
margins time_dummies, post noestimcheck
est store f_srh_c


/*-----------------------------------------------------------------------------
(IIc) Interaction models
------------------------------------------------------------------------------*/

* (i) MCS

// Without control variables
xtreg 	mcs 							///
		i.time_dummies##i.female,		///
	fe vce(robust)
margins time_dummies#female, post noestimcheck
est store i_mcs_nc

// Model 7: With control variables
xtreg 	mcs 							///
		i.time_dummies##i.female		///
		c.age##i.female					///
		c.gerlan##i.female				///
		i.legal_status##i.female		///
		i.accom##i.female,				///
	fe vce(robust)
est store m7
margins time_dummies#female, post noestimcheck
est store i_mcs_c


* (ii) Self-rated health status

// Without control variables
xtreg 	sr_health 						///
		i.time_dummies##i.female,		///
	fe vce(robust)
margins time_dummies#female, post noestimcheck
est store i_srh_nc

// Model 8: With control variables
xtreg 	sr_health 						///
		i.time_dummies##i.female		///
		c.age##i.female					///
		c.gerlan##i.female				///
		i.legal_status##i.female		///
		i.accom##i.female,				///
	fe vce(robust)
est store m8
margins time_dummies#female, post noestimcheck
est store i_srh_c


/*-----------------------------------------------------------------------------
(III) Graphs
------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
(IIIa) Figure 1
------------------------------------------------------------------------------*/

* (i) MCS; men and women combined; generated from Model 2

coefplot ///
	(o_mcs_c, label(Overall) offset(0) msymbol(O) mcolor(black) 			///
		msize(small) lcolor(black) lpattern(solid) lcolor(black) 			///
		ciopts(recast(rcap) lcolor(black) lwidth(thin)))      		 		///
	, title ("{bf:a)} Mental Health Component Score", 						///
			size(med) color(black))											///
			vertical recast(connected) 										///
			ytitle("MCS", size(small)) 										///
			ylab (45 (1) 52, labsize(small) angle(0) 						///
			grid glcolor(gs15) gmin gmax) 	 								///
			xtitle("{bf:Years Before / After Employment}", size(small)) 	///
			xscale(titlegap(1) ) xlab (,labsize(small) labgap(1) angle(0)) 	///
			legend(off) 	   												///
			graphregion(color(white) fcolor(white) icolor(white))     		///
			name(o_mcs_c, replace) xsize(4) note("")

* (ii) Self-rated health; men and women combined; generated from Model 4

coefplot ///
	(o_srh_c, label(Overall) offset(0) msymbol(O) mcolor(black) 			///
		msize(small) lcolor(black) lpattern(solid) lcolor(black) 			///
		ciopts(recast(rcap) lcolor(black) lwidth(thin)))      		 		///
	, title ("{bf:b)} Self-rated Current Health Status", 					///
			size(med) color(black))											///
			vertical recast(connected) 										///
			ytitle("Self-Rated Health", size(small)) 						///
			ylab (2.6 (0.1) 3.3, labsize(small) angle(0) 					///
			grid glcolor(gs15) gmin gmax) 									///
			xtitle("{bf:Years Before / After Employment}", size(small)) 	///
			xscale(titlegap(1) ) xlab (,labsize(small) labgap(1) angle(0)) 	///
			legend(off) 	   												///
			graphregion(color(white) fcolor(white) icolor(white))     		///
			name(o_srh_c, replace) xsize(4) note("")

* (iii) Combine panels into one graph			
			
graph combine o_mcs_c o_srh_c, graphregion(color(white)) caption("") xsize(6)
graph export "$graph/Figure1.png", replace		
	

/*-----------------------------------------------------------------------------
(IIIb) Figure 2
------------------------------------------------------------------------------*/

* (i) MCS; men and women separately; generated from Models 6 & 10

coefplot ///
	(f_mcs_c, label(Women) offset(0) msymbol(S) mcolor(maroon) msize(small) ///
		lcolor(maroon)  lpattern(solid) lcolor(maroon) 						///
		ciopts(recast(rcap) lcolor(maroon) lwidth(thin)))      		 		///
	(m_mcs_c, label(Men) offset(0) msymbol(T) mcolor(teal) msize(small)		///
		lcolor(teal)    lpattern(solid) lcolor(teal)  						///
		ciopts(recast(rcap) lcolor(teal) lwidth(thin)))	     				///
	, title ("{bf:a)} Mental Health Component Score", 						///
			size(med) color(black)) 										///
			vertical recast(connected) 									   	///
			ytitle("MCS", size(small))  									///
			ylab (42 (1) 58, labsize(small) angle(0) 						///
			grid glcolor(gs15) gmin gmax)    								///
			xtitle("{bf:Years Before / After Employment}", size(small)) 	///
			xscale(titlegap(1)) xlab (, labsize(small) labgap(1) angle(0)) 	///
			legend(on size(small)) 											///
			graphregion(color(white) fcolor(white) icolor(white))           ///
			name(mw_mcs_c, replace) note("") xsize(4)

* (ii) Self-rated health; men and women separately; generated from Models 8 & 12
			
coefplot ///
	(f_srh_c, label(Women) offset(0) msymbol(S) mcolor(maroon) msize(small) ///
		lcolor(maroon)  lpattern(solid) lcolor(maroon) 						///
		ciopts(recast(rcap) lcolor(maroon) lwidth(thin)))      		 		///
	(m_srh_c, label(Men) offset(0) msymbol(T) mcolor(teal) msize(small)		///
		lcolor(teal)    lpattern(solid) lcolor(teal)  						///
		ciopts(recast(rcap) lcolor(teal) lwidth(thin)))	     				///
	, title ("{bf:b)} Self-rated Current Health Status", 					///
			size(med) color(black)) 										///
			vertical recast(connected) 									   	///
			ytitle("Self-Rated Health", size(small))  						///
			ylab (2.2 (0.1) 3.3, labsize(small) angle(0) 					///
			grid glcolor(gs15) gmin gmax)    								///
			xtitle("{bf:Years Before / After Employment}", size(small)) 	///
			xscale(titlegap(1)) xlab (, labsize(small) labgap(1) angle(0)) 	///
			legend(off) 		   											///
			graphregion(color(white) fcolor(white) icolor(white))           ///
			name(mw_srh_c, replace) note("") xsize(4)

			
* (iii) Combine panels into one graph		
			
grc1leg2 mw_mcs_c mw_srh_c, graphregion(color(white)) 						///
	legendfrom(mw_mcs_c)													///
	caption("")	xsize(6)														
graph export "$graph/Figure2.png", replace	


/*-----------------------------------------------------------------------------
(IIIb) Figure 3
------------------------------------------------------------------------------*/

* (i) Barplot for first item at first observation

catplot female ega1 treated if n == 1, percent(female treated) 				///
	title("{bf:a)} {it:Even a married woman should have a paid job}" 		///
	"{it:so that she can be financially independent.}", 					///
	pos(12) span size(med) color(black)) 		  							///
	ytitle("{bf:Percent of respondents}",  size(small)) yscale(titlegap(1)) ///
	blabel(bar, format(%4.1f)) intensity(25) asyvars 						///
	bar(2, color(maroon) fintensity(inten80)) 								///
	bar(1, color(teal) fintensity(inten80)) 								///
	graphregion(color(white) fcolor(white) icolor(white)) 					///
	var3opts(label(ang(90) labsize(medsmall))) 								///
	var2opts(label(labsize(small)))											///
	ylab (, grid glcolor(gs15) gmin gmax)    								///
	name(ega1, replace) note("") xsize(4)

* (ii) Barplot for second item at first observation	
	
catplot female ega2 treated if n == 1, percent(female treated) 				///
	title("{bf:b)} {it:If a woman earns more money than her}" 				///
	"{it:        partner, this inevitably leads to problems.}",				///
	pos(12) ring() span size(med) color(black)) 		 					///
	ytitle("{bf:Percent of respondents}",  size(small)) yscale(titlegap(1)) ///
	blabel(bar, format(%4.1f)) intensity(25) asyvars 						///
	bar(2, color(maroon) fintensity(inten80)) 								///
	bar(1, color(teal) fintensity(inten80)) 								///
	graphregion(color(white) fcolor(white) icolor(white)) 					///
	var3opts(label(ang(90) labsize(medsmall))) 								///
	var2opts(label(labsize(small)))											///
	ylab (, grid glcolor(gs15) gmin gmax)    								///
	name(ega2, replace) note("") xsize(4)

* (iii) Combine panels into one graph		
	
grc1leg2 ega1 ega2, graphregion(color(white)) 						///
	legendfrom(ega1) xsize(6)										///
	caption("")	
graph export "$graph/Figure3.png", replace
	
	
/*-----------------------------------------------------------------------------
(IV) Regression tables
------------------------------------------------------------------------------*/

/*-----------------------------------------------------------------------------
(IVa) Table 1
------------------------------------------------------------------------------*/

esttab m1 m3 m5 m2 m4 m6 using "$table/table_1.tex", replace label 			///
	cells(b(fmt(2) star) se(fmt(2) par))									///
	star(* 0.10 ** 0.05 *** 0.01) 											///
	stats(r2 N N_g, fmt(2 0 0) labels(R-squared "Person-years" "Persons")) 	

/*-----------------------------------------------------------------------------
(IVb) Table A1
------------------------------------------------------------------------------*/
	
esttab m7 m8 using "$table/table_A1.tex", replace label 					///
	b(2) se(2) wide															///
	star(* 0.10 ** 0.05 *** 0.01) 											///
	stats(r2 N N_g, fmt(2 0 0) labels(R-squared "Person-years" "Persons")) 	
 
********************************************************************************
exit